![Test Game](https://gitlab.com/ZephaniahNoah/c-game/-/raw/master/textures/title.png)

# C Game
Just learning C by making a game engine.

# Build
* `make` will build the debug binary
* Do `make release` for a more optimized binary

# Assets
1. Textures:
	* The textures are raw image data in RGBA format.
	* That's four bytes per pixel.
	* The height of the image is determined by dividing the amount of pixels by the width.
	* The width must be defined when loading the texture. `loadTexture("example.data", 32);`
2. Sounds:
	* Nothing yet...
