#include <GL/glut.h>
#include <stdlib.h>
#include <string.h>
#include "code/physics.h"
#include "code/array.h"
#include <dirent.h>
#include <time.h>

#define minimapWidth 80
#define minimapHeight 50
const int tps = 60;
const int tickRate = 1000 / tps;
struct Texture titleTex;
struct Texture Tiles[tileCount];
struct Texture Walls[tileCount];
struct Texture player;
struct Texture grassOverlay;
struct Texture grass2Overlay;

// TODO: libasan -fsanitize=address
bool onMenu() {
	return onMainMenu || onOptionsMenu;
}

void drawWorld(int data[worldWidth][worldHeight], struct Texture textures[tileCount], bool isWalls) {
	float xT = (playerX / 16.0F);
	float yT = (playerY / 16.0F);

	int aX = (xT - round(xT)) * 16.0F;
	int aY = (yT - round(yT)) * 16.0F;

	for (int i = tileCount - 1; i > -1; i--) {
		for (int x = ceil(windowWidth / 16.0) + 1; x > -2; x--) {
			for (int y = -1; y < ceil(windowHeight / 16.0) + 2; y++) {
				int xPos = x * 16 - aX;
				int yPos = y * 16 + aY;
				int bx = toBlockX(xPos) - 1;
				int by = toBlockY(yPos) + worldHeight;

				if (!inBounds(bx, by)) {
					continue;
				}

				int tileId = data[bx][by];

				if (tileId == i + 1) {
					struct Texture tex = textures[tileId - 1];
					if (!blockRevealed(bx, by) && tileId == GRASS) {
						if (getNoise(bx, by) < .5)
							tex = grass2Overlay;
						else
							tex = grassOverlay;
					}
					glBindTexture(GL_TEXTURE_2D, tex.id);

					float edge = (tex.width - 16.0);
					float edge2 = (edge / 2.0);
					float frac = edge2 / tex.width;

					if (tex.width > 16) {
						for (int i = -1; i < 2; i++) {
							for (int j = -1; j < 2; j++) {
								int adjacentData = data[bx + i][by + j];

								if (isWalls) {
									int tile = getTile(bx + i, by + j);

									// If space is occupied by wall of same type or any tile
									if (adjacentData == tileId || (tile != 0 && tile != -1)) {
										// Don't render
										continue;
									}
								}

								bool flag = adjacentData == DIRT && tileId == GRASS;

								if (adjacentData != tileId && !flag) {
									int startX = edge2 * i - 8;
									int startY = edge2 * j - 8;

									int endX = edge2;
									int endY = edge2;

									if (i == 0)
										endX = 16;
									if (j == 0)
										endY = 16;
									if (i == 1)
										startX += 16 - edge2;
									if (j == 1)
										startY += 16 - edge2;

									float mod = (8 + edge2);
									float L = (float) (startX + mod) / tex.width;
									float R = L + (float) (endX) / tex.width;
									float T = (float) (startY + mod) / tex.width;
									float B = T + (float) (endY) / tex.width;

									drawTexC(xPos + startX, yPos + startY, endX, endY, white, L, R, T, B);
								}
							}
						}
					}

					if (isWalls && tiless[bx][by] != 0) {
						continue;
					}
					drawTexC(xPos - tex.width / 2 + edge2, yPos - tex.height / 2 + edge2, tex.width - edge, tex.height - edge, white, frac, 1 - frac, frac, 1 - frac);
				}
			}
		}
	}
}

int getMapColor(int X, int Y) {
	if (inBounds(X, Y)) {
		int tile = tiless[X][Y];
		if (tile == AIR)
			return getWallColor(wallss[X][Y]);
		return getTileColor(tile);
	}
	return 0;
}

void renderLoop() {
	if (onMenu()) {
		glClearColor(0.0f, 0.3f, 0.2f, 1.0f);
	} else {
		glClearColor(0.4804f, 0.6523f, 0.9257f, 1.0f);
	}
	glClear(GL_COLOR_BUFFER_BIT);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluOrtho2D(0.0, windowWidth, windowHeight, 0.0);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glEnable(GL_BLEND);

	int halfWit = windowWidth / 2;
	int halfHig = windowHeight / 2;

	if (onMenu()) {
		int width = 267;

		glEnable(GL_TEXTURE_2D);
		glBindTexture(GL_TEXTURE_2D, titleTex.id);
		drawTex((halfWit) - width / 2, halfHig - 120, width, 67);
		glDisable(GL_TEXTURE_2D);

		drawRect(windowWidth / 4, windowHeight / 4, halfWit, halfHig, 0x36AD82);
		int selectorWidth = halfWit - 20;
		int selectorHeight = 30;
		int totalItemsHeight = (maxMenuOptions + 1) * selectorHeight;
		int x = halfWit - selectorWidth / 2;
		int y = (halfHig - (totalItemsHeight / 2));
		drawRect(x, y + (selectedMenuOption * selectorHeight), selectorWidth, selectorHeight, 0x269D72);
		for (int i = 0; i < maxMenuOptions + 1; i++) {
			char *string = menuOptions[i];
			int e = (strlen(string) * charSize);
			int color = cyan;
			if (selectedMenuOption == i) {
				color = white;
			}
			drawFont(string, (halfWit) - (e / 2), y + (i * selectorHeight) + (selectorHeight / 2 - charSize / 2), color);
		}
	} else {
		if (bigMap) {
			// Big map
			for (int x = 0; x < windowWidth; x++) {
				for (int y = 0; y < windowHeight; y++) {
					drawRect(x, y, 1, 1, getMapColor(playerBlockX(0) + x - halfWit + xMapOffset, (playerBlockY(0) + y - halfHig + yMapOffset)));
				}
			}
		} else {
			xMapOffset = 0;
			yMapOffset = 0;

			glEnable(GL_TEXTURE_2D);

			// Draw walls
			drawWorld(wallss, Walls, True);

			// Draw tiles
			drawWorld(tiless, Tiles, False);

			// Render player
			glBindTexture(GL_TEXTURE_2D, player.id);
			drawTex(halfWit - 5, halfHig - 18, 10, 19);

			struct Texture texture = Tiles[selectedBlock];
			float edge = texture.width - 16.0F;
			float edge2 = edge / 2.0F;
			float frac = edge2 / texture.width;

			// Draw selected block
			glBindTexture(GL_TEXTURE_2D, texture.id);
			drawTexC(0, 0, 16, 16, white, frac, 1 - frac, frac, 1 - frac);

			glDisable(GL_TEXTURE_2D);

			// Mini map
			drawRect(windowWidth - minimapWidth - 2, 0, minimapWidth + 2,
			minimapHeight + 2, black);
			for (int x = 0; x < minimapWidth; x++) {
				for (int y = 0; y < minimapHeight; y++) {
					drawRect(x + windowWidth - minimapWidth - 1, y + 1, 1, 1, getMapColor(playerBlockX(0) + x - minimapWidth / 2, (playerBlockY(0) + y - minimapHeight / 2)));
				}
			}

			char pX[11];
			char pY[11];

			// Debug coords
			if (playerX > 0) {
				tostring(pX, playerX);
			} else if (playerX < 0) {
				int temp = playerX * -1;
				tostring(pX, temp);
				strcat(pX, "-");
			} else {
				strcpy(pX, "0");
			}
			if (playerY > 0) {
				tostring(pY, playerY);
			} else if (playerY < 0) {
				int temp = playerY * -1;
				tostring(pY, temp);
				strcat(pY, "-");
			} else {
				strcpy(pY, "0");
			}

			// Render coords
			drawFont(pX, 0, 0, white);
			drawFont(pY, 0, 16, white);
		}
	}
	glutSwapBuffers();
	// glDrawBuffer(0);
	GLenum err = glGetError();
	if (GL_NO_ERROR != err) {
		printf("Error %d\n", err);
		exit(EXIT_FAILURE);
	}
}

void gameLoop() {
	handleInput();

	if (!onMenu()) {
		gravity();
	}

	int seconds = time(NULL);

	// Run loop again...
	glutTimerFunc(tickRate, gameLoop, 0);
}

void loadBlocks(DIR *d, struct Texture textures[tileCount], char *folder) {
	struct dirent *dir;
	if (d) {
		while ((dir = readdir(d)) != NULL) {
			char *file = dir->d_name;
			if (file[0] != 46) {
				char string[6 + strlen(file)];
				strcpy(string, folder);

				const char *loc = strcat(string, file);
				const char *fileBytes = readTextureBytes(loc);

				struct Texture tex = loadTextureWithBytes(loc, -1, fileBytes);

				int index = file[0] - 49;

				textures[index] = tex;

				// If the texture array param is the wall array
				if (textures == &Walls) {
					int size = (tex.width * tex.height) / 4;

					unsigned char r[size];
					unsigned char g[size];
					unsigned char b[size];

					int colorCount = 0;

					for (int j = 0; j < size; j++) {

						// A
						unsigned char alphaC = fileBytes[j * 4 + 3];

						if (alphaC != 0) {

							// R
							unsigned char redC = fileBytes[j * 4];

							// G
							unsigned char greenC = fileBytes[j * 4 + 1];

							// B
							unsigned char blueC = fileBytes[j * 4 + 2];

							r[colorCount] = redC;
							g[colorCount] = greenC;
							b[colorCount] = blueC;

							colorCount++;
						}
					}

					int R = 0;
					int G = 0;
					int B = 0;

					for (int j = 0; j < colorCount; j++) {
						R += r[j];
						G += g[j];
						B += b[j];
					}

					R = R / colorCount;
					G = G / colorCount;
					B = B / colorCount;

					colors[index + 1] = (R << 16) | (G << 8) | B;
				}
			}
		}
		closedir(d);
	}
}

void initTextures() {
	titleTex = loadTexture("title.data", 267);
	player = loadTexture("man.data", 10);
	grassOverlay = loadTexture("grass.data", 22);
	grass2Overlay = loadTexture("grass2.data", 22);

	DIR *d;
	d = opendir("textures/tiles");
	loadBlocks(d, Tiles, "tiles/");
	d = opendir("textures/walls");
	loadBlocks(d, Walls, "walls/");
	initGraphics();
}

int main(int argc, char **argv) {
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE);
	// glutSetKeyRepeat(GLUT_KEY_REPEAT_OFF);
	glutInitWindowSize(windowWidth, windowHeight);
	glutCreateWindow("Test Game");
	initTextures();
	glutKeyboardFunc(keyboardPress);
	glutKeyboardUpFunc(keyboardRelease);
	glutMouseFunc(click);
	glutMotionFunc(motion);
	glutTimerFunc(0, fpsLimit, 0);
	glutTimerFunc(0, gameLoop, 0);
	glutDisplayFunc(renderLoop);
	glutMainLoop();

	return 0;
}
