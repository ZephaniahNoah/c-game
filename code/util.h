#include <sys/time.h>
#include <stdio.h>
#include <unistd.h>

long lastTextureLength;
const unsigned char False = 0;
const unsigned char True = 1;

int distance(float x1, float y1, float x2, float y2) {
	return sqrt(pow(x2 - x1, 2) + pow(y2 - y1, 2));
}

void tostring(char str[], int num) {
	int i, rem, len = 0, n;

	n = num;
	while (n != 0) {
		len++;
		n /= 10;
	}
	for (i = 0; i < len; i++) {
		rem = num % 10;
		num = num / 10;
		str[len - (i + 1)] = rem + '0';
	}
	str[len] = '\0';
}

long concatenate(long x, long y) {
	long pow = 10;
	while (y >= pow)
		pow *= 10;
	return x * pow + y;
}

long getTime() {
	struct timeval time;
	gettimeofday(&time, NULL);
	return concatenate(time.tv_sec, time.tv_usec);
}

char* readFileBytes(const char *name) {
	if (access(name, F_OK) != -1) {
		FILE *fl = fopen(name, "r");
		fseek(fl, 0, SEEK_END);
		lastTextureLength = ftell(fl);
		char *ret = malloc(lastTextureLength);
		fseek(fl, 0, SEEK_SET);
		fread(ret, 1, lastTextureLength, fl);
		fclose(fl);
		return ret;
	} else {
		printf("ERROR: Could not find %s\n", name);
		exit(0);
	}
}

char* convertIntegerToChar(int N) {

	int m = N;
	int digit = 0;
	while (m) {
		digit++;
		m /= 10;
	}

	char *arr;
	char arr1[digit];
	arr = (char*) malloc(digit);

	int index = 0;
	while (N) {
		arr1[++index] = N % 10 + '0';
		N /= 10;
	}

	int i;
	for (i = 0; i < index; i++) {
		arr[i] = arr1[index - i];
	}
	arr[i] = '\0';

	return (char*) arr;
}
