#include <GL/glut.h>
#include "graphics.h"
#include "world.h"

bool onMainMenu = True;
bool onOptionsMenu = False;
bool bigMap = False;
int selectedMenuOption = 0;
int xMapOffset = 0;
int yMapOffset = 0;
char *menuOptions[] = { "Play!", "Options", "Quit" };
const int maxMenuOptions = sizeof(menuOptions) / sizeof(menuOptions[0]) - 1;
bool holdUp = 0;
bool holdDown = 0;
bool holdLeft = 0;
bool holdRight = 0;

bool pressUp = False;
bool upState = True;
bool pressDown = False;
bool downState = True;
bool pressLeft = False;
bool leftState = True;
bool pressRight = False;
bool rightState = True;

void handleInput() {
	if (holdUp) {
		if (bigMap) {
			yMapOffset-=3;
		} else {
			if (upState) {
				pressUp = True;
			} else {
				pressUp = False;
			}
			upState = False;
		}
	} else {
		pressUp = False;
		downState = True;
	}
	if (holdDown) {
		if (bigMap) {
			yMapOffset+=3;
		} else {
			if (downState) {
				pressDown = True;
			} else {
				pressDown = False;
			}
			downState = False;
		}
	}
	if (holdLeft) {
		if (bigMap) {
			xMapOffset -= 3;
		} else {
			if (getTile(playerBlockX(11) - 2, playerBlockY(0)) == 0) {
				playerX--;
			}
		}
	}
	if (holdRight) {
		if (bigMap) {
			xMapOffset += 3;
		} else {
			if (getTile(playerBlockX(-10), playerBlockY(0)) == 0) {
				playerX++;
			}
		}
	}
}

void keyboardPress(unsigned char k, int x, int y) {
	bool up = k == 119 || k == 87;
	bool down = k == 115 || k == 83;
	bool left = k == 97 || k == 65;
	bool right = k == 100 || k == 68;
	bool m = k == 109 || k == 77;

	printf("Pressed %d\n", k);
	if (onMainMenu) {
		if (up) {
			selectedMenuOption--;
		} else if (down) {
			selectedMenuOption++;
		}
		if (selectedMenuOption < 0) {
			selectedMenuOption = maxMenuOptions;
		}
		if (selectedMenuOption > maxMenuOptions) {
			selectedMenuOption = 0;
		}
		if (k == 13) {
			switch (selectedMenuOption) {
			case 0:
				onMainMenu = 0;
				generateWorld();
				break;
			case 1:

				break;
			case 2:
				exit(0);
				break;
			}
		}
	} else if (onOptionsMenu) {
	} else {
		if (m) {
			bigMap = !bigMap;
		}

		if (up) {
			holdUp = 1;
		}
		if (down) {
			holdDown = 1;
		}
		if (left) {
			holdLeft = 1;
		}
		if (right) {
			holdRight = 1;
		}
	}
}

void keyboardRelease(unsigned char k, int x, int y) {
	bool up = k == 119 || k == 87;
	bool down = k == 115 || k == 83;
	bool left = k == 97 || k == 65;
	bool right = k == 100 || k == 68;

	if (up) {
		holdUp = 0;
	}
	if (down) {
		holdDown = 0;
	}
	if (left) {
		holdLeft = 0;
	}
	if (right) {
		holdRight = 0;
	}
}

int selectedBlock = 0;

void click(int button, bool release, int inx, int iny) {
	float pixelSizeX = (float) glutGet(GLUT_WINDOW_WIDTH) / (float) windowWidth;
	int x = ((float) inx / pixelSizeX);
	float pixelSizeY = (float) glutGet(GLUT_WINDOW_HEIGHT) / (float) windowHeight;
	int y = ((float) iny / pixelSizeY);
	if (!release && !onMainMenu) {
		int ex = toBlockX(x) - 1;
		int ey = toBlockY(y) + worldHeight;
		int px = playerBlockX(0);
		int py = playerBlockY(0);
		int dis = distance(ex, ey, px, py);
		if (dis < 4) {
			if (button == 0) {
				// Place tile
				setTile(ex, ey, selectedBlock + 1);
			}
			if (button == 1) {
				// Delete tile/wall
				if (getTile(ex, ey) == 0) {
					setWall(ex, ey, 0);
				} else {
					setTile(ex, ey, 0);
				}
			}
			if (button == 2) {
				// Place wall
				setWall(ex, ey, selectedBlock + 1);
			}
		}

		if (button == 3) {
			// Up
			if (selectedBlock < tileCount - 1) {
				selectedBlock++;
			}
		}
		if (button == 4) {
			// Down
			if (selectedBlock > 0) {
				selectedBlock--;
			}
		}
		printf("Mouse %d\n", button);
	}
}

void motion(int inx, int iny) {
	float pixelSizeX = (float) glutGet(GLUT_WINDOW_WIDTH) / (float) windowWidth;
	int x = ((float) inx / pixelSizeX);
	float pixelSizeY = (float) glutGet(GLUT_WINDOW_HEIGHT) / (float) windowHeight;
	int y = ((float) iny / pixelSizeY);
}
