#include <GL/glut.h>
#include <stdlib.h>
#include <stdio.h>
#include <stddef.h>
#include <string.h>
#include <math.h>
#include "types.h"
#include "util.h"

// 60 FPS
const int fps = 1000 / 60;
#define windowWidth 400.0
#define windowHeight 240.0
#define red 0xFF0000
#define green 0x00FF00
#define blue 0x0000FF
#define yellow 0xFFFF00
#define magenta 0xFF00FF
#define cyan 0x00FFFF
#define white 0xFFFFFF
#define charSize 18
#define black 0x000000
struct Texture font;

char* readTextureBytes(const char *name) {
	char string[9 + strlen(name)];
	strcpy(string, "textures/");
	return readFileBytes(strcat(string, name));
}

void fpsLimit() {
	glutPostRedisplay();
	glutTimerFunc(fps, fpsLimit, 0);
}

struct Texture loadTextureWithBytes(const char *file, int width, const char *fileBytes) {
	if (width == -1) {
		width = sqrt(lastTextureLength / 4);
	}

	const GLsizei w = width;
	const GLsizei h = (lastTextureLength / 4) / w;

	TextureID textureID = 0;
	glGenTextures(1, &textureID);
	glBindTexture(GL_TEXTURE_2D, textureID);
	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, w, h, 0, GL_RGBA, GL_UNSIGNED_BYTE, fileBytes);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);

	struct Texture tex = { textureID, w, h };

	// Free it? free(fileBytes);

	return tex;
}

struct Texture loadTexture(const char *file, int width) {
	const char *fileBytes = readTextureBytes(file);

	return loadTextureWithBytes(file, width, fileBytes);
}

int toColor(unsigned char r, unsigned char g, unsigned char b, unsigned char a) {
	return (a << 24) | (r << 16) | (g << 8) | b;
}

void initGraphics() {
	// Using https://gitlab.com/dwarf-fortress/taffer upscaled with XBR 2x
	font = loadTexture("taffer.data", 288);
}

void drawTexC(int x, int y, int width, int height, int c, float L, float R, float T, float B) {
	float b = c & 0xFF;
	float g = (c >> 8) & 0xFF;
	float r = (c >> 16) & 0xFF;

	r = (float) r / 255.f;
	g = (float) g / 255.f;
	b = (float) b / 255.f;

	// glColor3b doesn't work :/
	glColor3f(r, g, b);

	glBegin(GL_QUADS);

	// Top left
	glTexCoord2f(L, T);
	glVertex2i(x, y);

	// Top right
	glTexCoord2f(R, T);
	glVertex2i(x + width, y);

	// Bottom right
	glTexCoord2f(R, B);
	glVertex2i(x + width, y + height);

	// Bottom left
	glTexCoord2f(L, B);
	glVertex2i(x, y + height);

	glEnd();
}

void drawFont(char *text, int x, int y, int c) {
	float textureWidthInChars = 16.0f;
	int length = strlen(text);
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, font.id);
	for (int i = 0; i < length; i++) {
		float fraction = 1.0f / textureWidthInChars;
		float index = text[i];
		float left = fraction * fmod(index, textureWidthInChars);
		float right = left + fraction;
		float top = fraction * floor(index / textureWidthInChars);
		float bottom = top + fraction;
		drawTexC(x + (i * charSize), y, charSize, charSize, c, left, right, top, bottom);
	}
	glDisable(GL_TEXTURE_2D);
}

void drawRect(int x, int y, int width, int height, int c) {
	drawTexC(x, y, width, height, c, 0, 1, 0, 1);
}

void drawTex(int i, int j, int width, int height) {
	drawTexC(i, j, width, height, 0xFFFFFF, 0, 1, 0, 1);
}
