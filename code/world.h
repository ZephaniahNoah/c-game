#include <stdio.h>
#include "tiles.h"

#define worldWidth 1024
#define worldHeight 1024
#define dirtHeight 6
#define sampleRate 16 // Should be a power of 2
#define limitConstant 33.1595
#define SIZE 1000
#define PI 3.14159265359
int mid = worldHeight / 2;
int half = worldWidth / 2;
double samples[sampleRate + 1];
int beforeLast = 0;
int lastSample = 5;
int sample = 9;
int sampleCount = 3;
int tiless[worldWidth][worldHeight];
int wallss[worldWidth][worldHeight];
int playerX = ((worldWidth / 2) * 16) + 8;
int playerY = 0;

int playerBlockX(int mod) {
	return ceil((playerX + mod) / 16.0f);
}

int playerBlockY(int mod) {
	return ceil((playerY + mod) / 16.0f) * -1 + worldHeight;
}

int toBlockX(int x) {
	return ((x + playerX) - windowWidth / 2) / 16 + 1;
}

int toBlockY(int y) {
	return ((y - playerY) - windowHeight / 2) / 16 - 1;
}

bool coveredBy(int x, int y, int tileIid) {
	return getTile(x, y - 1) == tileIid && getTile(x, y + 1) == tileIid && getTile(x - 1, y) == tileIid && getTile(x + 1, y) == tileIid;
}

bool blockRevealed(int x, int y) {
	return getTile(x, y - 1) == AIR || getTile(x, y + 1) == AIR || getTile(x - 1, y) == AIR || getTile(x + 1, y) == AIR;
}

double reNoise(double x, double y) {
	double term = ((double) y * SIZE) / x;
	return (term * ((1000.0 * limitConstant) + .777));
}

double getNoise(double ix, double iy) {
	// Weird stuff happens at 0 0 so adding 300
	double x = ix + 1 + 300;
	double y = iy + 1;
	double ren = reNoise((y + (x / 2)), (x + (y / 2)));
	double men = ((y * ren) * SIZE);
	double term = ((men / (double) x) * limitConstant);
	return term - floor(term);
}

bool unfilled() {
	for (int i = 0; i < sampleRate + 1; i++) {
		if (samples[i] == -1) {
			return 1;
		}
	}
	return 0;
}

void smooth() {
	for (int i = 0; i < sampleRate + 1; i++) {
		samples[i] = -1;
	}
	samples[0] = lastSample;
	samples[sampleRate] = sample;

	while (unfilled()) {
		double first = -1;
		double second = -1;
		for (int i = 0; i < sampleRate + 1; i++) {
			// Get first and second index
			if (samples[i] == -1) {
				if (first == -1) {
					first = i - 1;
				}
			} else {
				if (first != -1) {
					second = i;
					break;
				}
			}
		}
		double index = first + ((second - first) / 2.);
		samples[(int) round(index)] = (samples[(int) round(first)] + samples[(int) round(second)]) / 2;
	}
}

int getHeighestTile(int x) {
	for (int y = 0; y < worldHeight; y++) {
		if (tiless[x][y] != AIR) {
			return y;
		}
	}
}

bool inBounds(int x, int y) {
	return (x > -1 && x < worldWidth - 1 && y > -1 && y < worldWidth - 1);
}

int getTile(int x, int y) {
	if (inBounds(x, y))
		return tiless[x][y];
	return -1;
}

int getWall(int x, int y) {
	if (inBounds(x, y))
		return wallss[x][y];
	return -1;
}

void setTile(int x, int y, int tile) {
	if (inBounds(x, y))
		tiless[x][y] = tile;
}

void setWall(int x, int y, int wall) {
	if (inBounds(x, y))
		wallss[x][y] = wall;
}

int sign(int i) {
	return (signbit((float) i) * -1) * 2 + 1;
}

void makeCircle(int x, int y, int size, int tileId, bool tile) {
	if (size == 0) {
		if (tile)
			setTile(x, y, tileId);
		else
			setWall(x, y, tileId);
	}
	for (int X = -size; X < size; X++) {
		for (int Y = -size; Y < size; Y++) {
			if (distance(x - .5 * sign(x), y - .5 * sign(y), x + X, y + Y) < size) {
				if (tile)
					setTile(x + X, y + Y, tileId);
				else
					setWall(x + X, y + Y, tileId);
			}
		}
	}
}

void carveCircle(int x, int y, int size, bool tile) {
	makeCircle(x, y, size, AIR, tile);
}

double CosineInterpolate(double y1, double y2, double mu) {
	double mu2 = (1 - cos(mu * PI)) / 2;
	return (y1 * (1 - mu2) + y2 * mu2);
}

void blob(int x, int y, float noise, int type, bool doWalls, int multiplier, int addition, double from, double to, int blobSize, float spread) {
	if (y > mid + 50) {
		if (noise < to && noise > from) {
			float l = getNoise(x + 1, y);
			int blobCount = l * multiplier + addition;

			for (int i = 0; i < blobCount; i++) {
				float xOffset = getNoise(y, x + i) * spread;

				float yOffset = getNoise(y + i, x) * spread;

				float size = getNoise(x + i, y + xOffset) * blobSize;
				makeCircle(x + xOffset, y + yOffset, size, type, True);
				if (doWalls) {
					makeCircle(x + xOffset, y + yOffset, size, type, False);
				}
			}
		}
	}
}

void generateWorld() {
	printf("Generating world %d high and %d wide...\n", worldHeight, worldWidth);
	for (int x = 0; x < worldWidth; x++) {
		double noiseX = getNoise(worldWidth / sampleRate, x + 1) * 2;
		if (sampleCount >= sampleRate) {
			beforeLast = lastSample;
			lastSample = sample;
			sample = floor(noiseX * 10) * (noiseX - (noiseX / 8));
			int av = (beforeLast + lastSample + sample) / 3;
			sample = ((av + sample) / 2);
			sampleCount = 0;
			smooth();
		}
		sampleCount++;

		int height = 5;

		int limit = CosineInterpolate(beforeLast, lastSample, (float) sampleCount / (float) sampleRate);

		for (int y = 0; y < worldHeight; y++) {
			if (y >= mid) {
				if (y >= mid + limit) {
					if (y == mid + limit) {
						for (int i = 0; i < dirtHeight; i++) {
							double noise = getNoise(x, y - i);
							int tile = DIRT;
							if (noise > .85) {
								tile = GRASS;
							}
							tiless[x][y - i] = tile;
							wallss[x][y - i] = DIRT;
						}
						tiless[x][y - dirtHeight] = GRASS;

						if (x == half) {
							playerY = ((worldHeight - (y - dirtHeight - 1)) - 1) * 16 + 1;
						}
					} else {
						wallss[x][y] = tiless[x][y] = STONE;
					}
				}
			}
		}
	}

	// Blobs
	for (int x = 0; x < worldWidth; x++) {
		for (int y = 0; y < worldHeight; y++) {
			double noise = getNoise(x, y);
			int n = floor(noise);
			noise = noise - n;

			// Iron ore
			blob(x, y, noise, IRON_ORE, False, 6, 3, .00511, .00519, 3, 4.f);

			// Dirt
			blob(x, y, noise, DIRT, True, 10, 5, .0058, .0059, 4, 6.f);
		}
	}

	// Cave thingies
	for (int x = 0; x < worldWidth; x++) {
		double noiseX = getNoise(worldWidth / sampleRate, x + 1) * 2;
		int y = getHeighestTile(x);

		if (noiseX > 1 && noiseX < 1.01) {
			for (int i = -16; i < 16; i++) {
				for (int j = -16; j < 16; j++) {
					if (inBounds(x + i, y + j)) {
						int dis = distance(x, y, x + i, y + j);
						double noise = getNoise(x + i, y + j);
						bool flag = (dis == 15 && noise > .5);
						if ((dis < 15 || flag)) {
							int tile = getTile(x + i, y + j);
							bool flag2 = tile == AIR || tile == GRASS;
							if (dis > 5 || (dis == 5 && noise > .5)) {
								if (flag2) {
									if (flag || dis == 14) {
										setTile(x + i, y + j, GRASS);
									} else {
										setTile(x + i, y + j, DIRT);
									}
								}
							} else {
								setTile(x + i, y + j, AIR);
							}
							if (dis < 14 && flag2)
								setWall(x + i, y + j, DIRT);
						}
					}
				}
			}

			bool tunnel = False;

			if (inBounds(x + 18, y)) {
				if (tiless[x + 18][y] == AIR) {
					for (int i = 0; i < 6; i++) {
						carveCircle(x + i * 4, y, 3, True);
					}
					tunnel = True;
				}
			}
			if (inBounds(x - 18, y)) {
				if (tiless[x - 18][y] == AIR) {
					for (int i = 0; i < 6; i++) {
						carveCircle(x - i * 4, y, 3, True);
					}
					tunnel = True;
				}
			}

			int dir = 1;
			if (!tunnel) {
				float n = getNoise(x, 0);
				float n2 = (n * 2.0) - 1;

				if (n2 > 0)
					dir = 1;
				else
					dir = -1;

				if (getTile(x + 18 * dir, y - 18) == AIR) {
					tunnel = True;
				} else {
					dir *= -1;
					if (getTile(x + 18 * dir, y - 18) == AIR) {
						tunnel = True;
					}
				}

				if (tunnel) {
					int j = 0;
					for (int i = 0; i > -6; i -= 1) {
						carveCircle(x + j * 2, y + i * 2, 4, True);
						j += dir;
					}
				}
			}

			for (int i = 0; i < 3; i++) {
				carveCircle(x, y + (i * 5), 3, True);
			}

			float n = getNoise(x, y);
			int branchCount = (n * 19.0) + 3;
			int Y = y + 15;
			int X = x;

			for (int i = 0; i < branchCount; i++) {
				float noise = getNoise(X + i, Y);
				int branchSegments = 2 + (noise * 6);
				float n2 = (noise * 2.0) - 1;
				dir;

				if (n2 > 0)
					dir = 1;
				else
					dir = -1;

				for (int j = 0; j < branchSegments; j++) {
					for (int k = 0; k < 3; k++) {
						float n3 = getNoise(X, Y);
						int down = round(n3 * 2);
						carveCircle(X, Y, 3 + down, True);
						Y += 1 + down;
						X += (3 * dir);
					}
				}
			}

			x += 200;
		}
	}
	// Trees
	for (int x = 0; x < worldWidth; x++) {
		float noise = getNoise(x, 55);

		if (noise < .03) {
			int y = getHeighestTile(x);
			int treeHeight = ceil(noise * 250) + 1;
			for (int i = 0; i < treeHeight; i++) {
				setWall(x, y - 1 - i, LOG);
			}
			setWall(x, y - 1 - treeHeight, TREE);
			setWall(x, y - 1, LOG);
		}
	}
}
