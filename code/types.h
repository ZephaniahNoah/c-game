#pragma once
typedef _Bool bool;
typedef GLuint TextureID;
struct Texture {
	TextureID id;
	int width;
	int height;
};
