#define tileCount 8
#define AIR 0
#define STONE 1
#define DIRT 2
#define GRASS 3
#define PLANK 4
#define LOG 5
#define LEAVES 6
#define TREE 7
#define IRON_ORE 8
int colors[tileCount + 1];

int getWallColor(int tile) {
	if (tile > 0 && tile < tileCount + 1) {
		return colors[tile];
	}
	return 0xFF7BA6EC;
}

// Tile colors are just wall colors but brighter
int getTileColor(int tile) {
	if (tile == AIR)
		return getWallColor(AIR);

	int c = getWallColor(tile);

	int b = c & 0xFF;
	int g = (c >> 8) & 0xFF;
	int r = (c >> 16) & 0xFF;

	r = r * 1.3F;
	g = g * 1.3F;
	b = b * 1.3F;

	return (r << 16) | (g << 8) | b;
}
