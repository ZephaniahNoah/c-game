#include "input.h"

const char fallSpeedLimit = -6;
int timer = 0;
int speed = 0;
int jumpHeight = 2;

bool emptyBelow() {
	int tile = getTile(playerBlockX(0) - 1, playerBlockY(-1));
	return tile == 0;
}

void fall() {
	if (timer > 7) {
		timer = 0;
		if (speed >= fallSpeedLimit) {
			speed--;
		}
	}

	timer++;

	if (speed > -1) {
		// Moving up
		playerY += speed;
	} else {
		// Moving down
		for (int i = 0; emptyBelow() && i > speed; i--) {
			playerY--;
		}
	}
}

void gravity() {
	if (holdUp && !emptyBelow() && !bigMap) {
		speed = jumpHeight;
	}

	fall();

	if (!emptyBelow()) {
		speed = 0;
		timer = 0;
	}
}
