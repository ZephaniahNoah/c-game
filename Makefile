CC=gcc
CFLAGS=-g
EXECS=main

all:
	$(CC) $(CFLAGS) main.c -lglut -lGLU -lGL -lc -lm -o main

release:
	$(CC) $(CFLAGS) main.c -lglut -lGLU -lGL -lc -lm -O -O1 -O3 -Os -o release

clean:
	rm -f release main